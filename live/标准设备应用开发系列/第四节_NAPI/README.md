## 标准设备应用开发—NAPI

#### 一、简介
本项目为《标准设备应用开发—NAPI》课程的相关示例源码。

#### 二、课程PPT

阿里云盘：

百度网盘：

#### 三、代码说明

说明：**下面代码基于v3.1-Beta版本OpenHarmony系统源码，并在rk3568、Hi3516DV300开发板均已验证（后面以rk3568为例说明）。master分支及新版本分支可能会存在部分差异。**

示例源码目录结构：
```sh
+--- @ohos.mynapidemo.d.ts							//接口定义.d.ts文件
+--- index.ets										//音乐播放器代码，在其中添加开关LED等闪灯效果代码
+--- ohosCode										//目录，包含OpenHarmony系统源码下新增或修改内容
|   +--- build
|   |   +--- subsystem_config.json					//添加mysubsys子系统配置
|   +--- device
|   |   +--- hihope
|   |   |   +--- rk3568
|   |   |   |   +--- build
|   |   |   |   |   +--- rootfs
|   |   |   |   |   |   +--- init.rk3568.cfg		//rk3568开发板，添加应用访问相关export文件的权限
|   |   +--- hisilicon
|   |   |   +--- hi3516dv300
|   |   |   |   +--- build
|   |   |   |   |   +--- rootfs
|   |   |   |   |   |   +--- init.Hi3516DV300.cfg	//Hi3516DV300，添加应用访问相关export文件的权限
|   +--- mysubsys
|   |   +--- mynapipart
|   |   |   +--- mynapidemo
|   |   |   |   +--- BUILD.gn						//mynapidemo动态库的的gn构建配置
|   |   |   |   +--- mynapidemo.cpp					//NAPI接口实现C++源码
|   |   +--- ohos.build								//mysubsys的配置文件，定义组件及其module
|   +--- productdefine
|   |   +--- common
|   |   |   +--- products
|   |   |   |   +--- Hi3516DV300.json				//Hi3516DV300开发板产品定义文件，添加mynapipart组件
|   |   |   |   +--- rk3568.json					//rk3568开发板产品定义文件，添加mynapipart组件
+--- README.md
```

##### NAPI扩展动态库代码

[ohosCode](ohosCode/)目录存放本课程新增的NAPI动态扩展库源码及对应的构建配置。NAPI动态扩展库的代码为新增的子系统、组件，较为独立，放在[ohosCode/mysubsys](ohosCode/mysubsys)目录下，如下载的是v3.1-Beta版本的OpenHarmony源码，可直接复制整个目录到OpenHarmony源码根目录下，无需修改。

##### 添加子系统

修改子系统配置文件\${OHOS_REPO}/build/subsystem_config.json（\${OHOS_REPO}表示OpenHarmony源码根目录，下同），添加mysubsys子系统，包含名称和路径（相对根目录）。

```json
{
  ###### 新增内容开始（复制时不要包含此行，注意行尾逗号，确保文件格式正确） ######
  "mysubsys": {
    "path": "mysubsys",
    "name": "mysubsys"
  },
  ###### 新增内容结束（复制时不要包含此行） ######
  "ace": {
    "path": "foundation/ace",
    "name": "ace"
  },
  ...
}
```

##### 修改产品定义文件
修改产品定义文件\${OHOS_REPO}/productdefine/common/products/\${product-name}.json（${product-name}为具体产品的名称），添加包含新增的组件，新增一行配置“"mysubsys:mynapipart":{},”。
修改productdefine/common/products/rk3568.json文件，修改配置添加组件mynapipart。
```json
{
  "product_name": "rk3568",
  "product_company": "hihope",
  "product_device": "rk3568",
  "version": "2.0",
  "type": "standard",
  "product_build_path": "device/hihope/build",
  "parts":{
    ###### 新增内容开始（复制时不要包含此行，注意行尾逗号，确保文件格式正确） ######
    "mysubsys:mynapipart":{},
    ###### 新增内容结束（复制时不要包含此行） ######
    "ace:ace_engine_standard":{},
    ...
}
```

##### 修改文件权限
本例开关LED需要授予应用访问LED相关export文件的权限。修改${OHOS_REPO}/device/hihope/rk3568/build/rootfs/init.rk3568.cfg 文件，在boot的cmds中添加相关命令。

```json
        }, {
            "name" : "boot",
            "cmds" : [
                "chmod 777 /dev/ttyAMA2",
                "chmod 775 /sys/class/rfkill/rfkill0/state",
                "chmod 777 /dev/rtkbt_dev",
                "chmod 0440 /proc/interrupts",
                "chmod 0440 /proc/stat",
                "chmod 0640 /dev/xt_qtaguid",
                "chmod 0660 /proc/net/xt_qtaguid/ctrl",
                "chmod 0440 /proc/net/xt_qtaguid/stats",
                "chmod 666 /dev/mali0",
                "chown system graphics /dev/mali0",
                "chown system graphics /dev/graphics/fb0",
                "chmod 666 /dev/ion",
                "chown system system /dev/ion",
                ###### 新增内容开始（复制时不要包含此行，注意行尾逗号，确保文件格式正确） ######
                "chmod 666 /sys/class/leds/red/brightness",
                "chmod 666 /sys/class/leds/red/trigger",
                "chmod 666 /sys/class/leds/green/brightness",
                "chmod 666 /sys/class/leds/green/trigger",
                "chmod 666 /sys/class/leds/blue/brightness",
                "chmod 666 /sys/class/leds/blue/trigger"
                ###### 新增内容结束（复制时不要包含此行） ######
            ]
        }
    ]
}
```

##### 应用调用接口
首先复制[@ohos.mynapidemo.d.ts](@ohos.mynapidemo.d.ts) 到OpenHarmony SDK安装目录的ets/\${ets_version}/api/common子目录下。\${ets_version}和OpenHarmony SDK的版本对应关系：
- SDK7： 3.0.0.0
- SDK8： 3.1.0.0

OpenHarmony SDK安装在默认路径%LocalAppData%\OpenHarmony\Sdk目录，如使用SDK8，则复制[@ohos.mynapidemo.d.ts](@ohos.mynapidemo.d.ts) 文件到%LocalAppData%\OpenHarmony\Sdk\ets\3.1.0.0\api\common\ets\3.1.0.0\api\common目录下。


本例在[分布式音乐播放器](FA\Entertainment\DistrubutedMusicPlayer)中调用NAPI接口，实现播放音乐时加上闪灯效果。先下载好[分布式音乐播放器](FA\Entertainment\DistrubutedMusicPlayer) 的代码。打开src/main/ets/MainAbility/pages/index.ets文件，添加代码import mynapidemo模块。

```typescript
import mynapidemo from '@ohos.mynapidemo'
```

再新增2个方法，封装开启、关闭红色LED灯，每款开发板LED灯的pin值都不同，具体值参考开发板的说明。

```typescript
  addLedEffect(){
    mynapidemo.setLedStatus(147, mynapidemo.LED_ON).then((result: number) => {
      if (result === 0){
        CommonLog.info("Turned on LED.")
      } else {
        CommonLog.info("Turned on LED failed.")
      }
    })
  }

  removeLedEffect(){
    mynapidemo.setLedStatus(147, mynapidemo.LED_OFF).then((result: number) => {
      if (result === 0){
        CommonLog.info("Turned off LED.")
      } else {
        CommonLog.info("Turned off LED failed.")
      }
    })
  }
```

接着找到onPlayOrPauseMusic()方法，播放音乐时加上LED灯效果，暂停时去掉。

```typescript
  onPlayOrPauseMusic() {
    if (this.isSwitching) {
      CommonLog.info('onPlayOrPauseMusic ignored, isSwitching')
    }
    this.isSwitching = true

    if (this.isPlaying) {
      CommonLog.info("Start to pause music")
      this.playerManager.pause()
      this.pauseAnimation()
      this.removeLedEffect() //暂停时去掉LED闪灯效果
    }
    else {
      CommonLog.info("Start to play music")
      this.playerManager.play(-1)
      this.playAnimation()
      this.addLedEffect() //播放时加上LED闪灯效果
    }
    this.isPlaying = !this.isPlaying
    this.isSwitching = false
  }
```