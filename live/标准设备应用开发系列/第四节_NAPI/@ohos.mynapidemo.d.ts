/*
* Copyright (c) 2021 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

/**
 * my napi demo
 * @since 7
 * @devices phone, tablet
 * @import import mynapidemo from '@ohos.mynapidemo';
 * @permission N/A
 */

declare namespace mynapidemo {
	const LED_ON = 1;
	const LED_OFF = 0;
	/**
	 * 计算2个数之和
	 * @since 7
	 * @param x 被加数
	 * @param y 加数
	 * @return 和
	 */
	function add(x: number, y: number): number;

	/**
	 * 设置LED灯的开关状态
	 * @param pin pin口号
	 * @param status 状态，1：开启；0：关闭；
	 * @param callback 	回调函数
	 */
	function setLedStatusWithCallback(pin: number, status: number, callback: (result: number) => void): void;

	/**
	 * 设置LED灯的开关状态
	 * @param pin pin口号
	 * @param status 状态，1：开启；0：关闭；
	 */
	function setLedStatusWithPromise(pin: number, status: number): Promise<number>;

	/**
	 * 设置LED灯的开关状态
	 * @param pin pin口号
	 * @param status 状态，1：开启；0：关闭；
	 * @param callback 	回调函数
	 */
	function setLedStatus(pin: number, status: number, callback: (result: number) => void): void;

	/**
	 * 设置LED灯的开关状态
	 * @param pin pin口号
	 * @param status 状态，1：开启；0：关闭；
	 */
	function setLedStatus(pin: number, status: number): Promise<number>;
}

export default mynapidemo;