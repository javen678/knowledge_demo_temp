## 标准设备应用开发—动画与音乐

#### 一、简介
本项目为《动画与音乐》课程的项目源码，可实现音乐播放和动画效果。

#### 二、课程PPT

阿里云盘：

百度网盘：

#### 三、课程代码笔记
##### 唱盘指针动画

1. 声明@State变量

```
@State pointerAngle: number = 0 // 指针旋转角度
@State turntableAngle: number = 0 // 唱盘旋转角度
private turntableTimer: number // 唱盘定时器
```

1. 加入唱盘指针组件

```
Image($r("app.media.img_pointer"))
    .objectFit(ImageFit.Contain)
    .aspectRatio(1.32)
    .width('40%')
    .alignSelf(ItemAlign.Start)
    .rotate({
        x: 0,
        y: 0,
        z: 1,
        centerX: 0,
        centerY: 0,
        angle: this.pointerAngle
    })
    .offset({ x: '50%', y: '0%' })
```

1. 编写动画方法

```
playAnimation() {
//    if (this.turntableTimer) {
//        return
//    }
    // 唱盘转动动画
    let fun = () => {
        animateTo({ duration: 1500, curve: Curve.Linear, playMode: PlayMode.Normal },
            () => {
                this.turntableAngle = this.turntableAngle + 90;
            })
    }
    fun()
//    this.turntableTimer = setInterval(fun, 1500)
    // 唱盘指针下拨动画
    animateTo({ duration: 1000, curve: Curve.EaseInOut, playMode: PlayMode.Normal },
        () => {
            this.pointerAngle = 39;
        })
}

// 暂停音乐动画
pauseAnimation() {
    // 清除唱盘转动定时器
//    if (this.turntableTimer) {
//        this.turntableAngle = this.turntableAngle
//        clearInterval(this.turntableTimer)
//        this.turntableTimer = undefined
//    }
    // 唱盘指针上拨动画
    animateTo({ duration: 1500, curve: Curve.EaseInOut, playMode: PlayMode.Normal },
        () => {
            this.pointerAngle = 0;
        })
}
```

4.编写播放事件并绑定

```
// 播放或暂停音乐
onPlayOrPauseMusic() {
    if (this.isPlaying) {
        CommonLog.info("Start to pause music")
        this.pauseAnimation()
    }
    else {
        CommonLog.info("Start to play music")
        this.playAnimation()
    }
    this.isPlaying = !this.isPlaying
}
```

##### 音乐播放

进度条拖动

```
// 音乐进度变化
    onChangeMusicProgress(value: number, mode: SliderChangeMode) {
        if (this.isSwitching) {
            CommonLog.info('onChangeMusicProgress ignored, isSwitching');
            return;
        }
        this.isSwitching = true
        CommonLog.info('value:' + value + 'mode:' + mode.toString())
        this.currentProgress = value
        if (this.totalTimeText != '00:00') {
            let currentMS = this.currentProgress / 100 * this.playerManager.getTotalTimeMs()
            this.currentTimeText = this.getShownTimer(currentMS)
            CommonLog.info('onChangeMusicProgress:' + currentMS)
            this.playerManager.seek(currentMS)
        } else {
            CommonLog.info('onChangeMusicProgress:bbb')
            this.currentProgress = 0
        }
        this.isSwitching = false
    }
```

