#  Hi3516DV300开发板标准设备应用开发HelloWorld


## 1、简介
本样例基于Hi3516DV300开发板，旨在告诉读者如何新建Helloworld项目、将生成的固件包烧录到Hi3516DV300开发板。

#### 1.1.效果
本样例总体运行效果，如图：

&nbsp;![1](./resource/1.jpg)

#### 1.2.支持OpenHarmony版本

OpenHarmony 3.0 LTS、OpenHarmony 3.1 Beta。


#### 1.3.支持开发板

润和HiSpark Taurus AI Camera(Hi3516d)开发板套件


## 2、 准备编译环境

搭建开发环境有windows和Ubuntu两种，这里以搭建Ubuntu环境(Docker方式)为例

#### 获取Docker环境

```
#安装Docker
cd ~
curl -fsSL https://get.docker.com -o get-docker.sh
chmod a+x get-docker.sh
sudo ./get-docker.sh

#获取Docker镜像
sudo docker pull swr.cn-south-1.myhuaweicloud.com/openharmony-docker/openharmony-docker-standard:0.0.5

#进入源码根目录执行如下命令，从而进入Docker构建环境
sudo docker run -it -v $(pwd):/home/openharmony swr.cn-south-1.myhuaweicloud.com/openharmony-docker/openharmony-docker-standard:0.0.5
```


## 3、 准备源码

#### 3.1.配置git和安装repo

参考[官网文章](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.0-LTS/zh-cn/device-dev/quick-start/quickstart-standard-docker-environment.md) 步骤学习

```
#安装Ubuntu 20.04

# 安装repo
curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 | sudo tee /usr/local/bin/repo >/dev/null
sudo chmod a+x /usr/local/bin/repo

# 设置git信息
git config --global user.name "yourname"
git config --global user.email "your-email-address"
git config --global credential.helper store

#安装git-lfs
sudo apt-get install git-lfs
```

#### 3.2.拉取源码

```
cd ~
mkdir openharmony
cd openharmony
repo init -u https://gitee.com/openharmony/manifest.git -b refs/tags/OpenHarmony-v3.1-Beta --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```
#### 3.3.执行prebuilts

```
#在源码根目录下执行脚本，安装编译器及二进制工具
#下载的prebuilts二进制默认存放在与OpenHarmony同目录下的OpenHarmony_2.0_canary_prebuilts下
./build/prebuilts_download.sh
```

## 4、 源码编译

```
#进入源码根目录，执行如下命令进行版本编译
./build.sh --product-name Hi3516DV300 --ccache

```

## 5、镜像烧录
USB烧录

#### 5.1、请连接好电脑和待烧录开发板，需要同时连接串口和USB口，具体可参考[Hi3516DV300开发板介绍](https://gitee.com/openharmony/docs/blob/OpenHarmony_1.0.1_release/zh-cn/device-dev/quick-start/Hi3516%E5%BC%80%E5%8F%91%E6%9D%BF%E4%BB%8B%E7%BB%8D.md)


&nbsp;![3](./resource/3.png)


#### 5.2、打开电脑的设备管理器，查看并记录对应的串口号。
如果对应的串口异常，请根据[Hi3516DV300/Hi3518EV300开发板串口驱动安装指导](https://device.harmonyos.com/cn/docs/documentation/guide/hi3516_hi3518-drivers-0000001050743695) 安装USB转串口的驱动程序。

&nbsp;![4](./resource/4.png)

#### 5.3、安装USB驱动

下载USB驱动，[下载地址](http://www.hihope.org/download/download.aspx?mtt=11)


&nbsp;![2](./resource/2.png)

如果安装驱动后，还提示不识别的USB设备，请将下面的内容保存未一个注册表文件，如test.reg，然后注册。


Windows Registry Editor Version 5.00

```
[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\usbflags\12D1D0010100]
"SkipBOSDescriptorQuery"=hex:01,00,00,00
"osvc"=hex:00,00
"IgnoreHWSerNum"=hex:01
```

#### 5.4、使用Hitools，烧写，然后按住串口边上的复位键，插拔USB电源线重启

&nbsp;![5](./resource/5.png)


#### 5.5、烧写成功

&nbsp;![6](./resource/6.png)


## 6、镜像运行

第一次烧录3.1镜像后，启动时需要设置，参考官网中给的[命令](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.0-LTS/zh-cn/device-dev/quick-start/quickstart-standard-running.md) 设置（仅第一次烧录时需要）。使用方式：在终端1窗口设置正确端口号，如COM3，点击连接。重启开发板电源时，迅速按回车，输入如下命令


&nbsp;![24](./resource/24.jpg)

```
setenv bootargs 'mem=640M console=ttyAMA0,115200 mmz=anonymous,0,0xA8000000,384M clk_ignore_unused rootdelay=10 hardware=Hi3516DV300 init=/init root=/dev/ram0 rw blkdevparts=mmcblk0:1M(boot),15M(kernel),20M(updater),2M(misc),3307M(system),256M(vendor),-(userdata)';

setenv bootcmd 'mmc read 0x0 0x82000000 0x800 0x4800; bootm 0x82000000'

save

reset

```

## 7、创建应用

### 7.1、下载DevEco Studio [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta)

### 7.2、配置SDK，参考[配置OpenHarmony-SDK](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md)

### 7.3、创建应用

###### 7.3.1、选择模板
打开DevEco Studio，创建一个新工程，在DevEco Studio中点击File -> New Project，选择模板[Standard]Empty Ability。

&nbsp;![13](./resource/13.jpg)

###### 7.3.2、工程配置

进入配置工程界面，Project Type选择Application，Development mode选择Traditional coding,Language 选择eTS语言，其他参数根据实际需要设置即可，最后点击Finish即创建成功。

&nbsp;![13_1](./resource/13_1.jpg)

### 7.4、配置签名信息
说明：使用真机设备运行和调试OpenHarmony应用前，需要对应用进行签名才能正常运行

###### 7.4.1、生成密钥和证书请求文件

- 1.在主菜单栏点击Build > Generate Key and CSR。

- 2.在Key Store File中，可以点击Choose Existing选择已有的密钥库文件（存储有密钥的.p12文件）；如果没有密钥库文件，点击New进行创建。下面以新创建密钥库文件为例进行说明。

&nbsp;![14](./resource/14.jpg)

- 3.在Create Key Store窗口中，填写密钥库信息后，点击OK。

  Key Store File：选择密钥库文件存储路径。

  Password：设置密钥库密码，必须由大写字母、小写字母、数字和特殊符号中的两种以上字符的组合，长度至少为8位。请记住该密码，后续签名配置需要使用。

  Confirm Password：再次输入密钥库密码。

&nbsp;![15](./resource/15.jpg)

- 4.在Generate Key界面中，继续填写密钥信息后，点击Next。

  Alias：密钥的别名信息，用于标识密钥名称。请记住该别名，后续签名配置需要使用。

  Password：密钥对应的密码，与密钥库密码保持一致，无需手动输入。

  Validity：证书有效期，建议设置为25年及以上，覆盖应用/服务的完整生命周期。

  Certificate：输入证书基本信息，如组织、城市或地区、国家码等。

&nbsp;![16](./resource/16.jpg)

- 5.在Generate CSR界面，选择密钥和设置CSR文件存储路径，然后点击ok,创建CSR文件成功。

&nbsp;![17](./resource/17.jpg)

###### 7.4.2、生成应用证书文件

进入DevEco Studio安装目录的Sdk\toolchains\lib文件夹下（该SDK目录只能是OpenHarmony SDK，配置方法可参考配置OpenHarmony SDK），打开命令行工具，执行如下命令（如果keytool命令不能执行，请在系统环境变量中添加JDK的环境变量）。其中，只需要修改输入和输出即可快速生成证书文件，即修改**-infile指定证书请求文件csr文件路径，-outfile**指定输出证书文件名及路径。

```
keytool -gencert -alias "OpenHarmony Application CA" -infile myApplication_ohos.csr -outfile myApplication_ohos.cer -keystore OpenHarmony.p12 -sigalg SHA384withECDSA -storepass 123456 -ext KeyUsage:"critical=digitalSignature" -validity  3650 -rfc
```

关于该命令的参数说明如下：

- alias：用于签发证书的CA私钥别名，OpenHarmony社区CA私钥存于OpenHarmony.p12密钥库文件中，该参数不能修改。

- infile：证书请求（CSR）文件的路径。

- outfile：输出证书链文件名及路径。

- keystore：签发证书的CA密钥库路径，OpenHarmony密钥库文件名为OpenHarmony.p12，文件在OpenHarmony SDK中Sdk\toolchains\lib路径下，该参数不能修改。请注意，该OpenHarmony.p12文件并不是生成密钥和证书请求文件中生成的.p12文件。

- sigalg：证书签名算法，该参数不能修改。

- storepass：密钥库密码，密码为123456，该参数不能修改。

- ext：证书扩展项，该参数不能修改。

- validity：证书有效期，自定义天数。

- rfc：输出文件格式指定，该参数不能修改。

###### 7.4.3、生成应用Profile文件

Profile文件包含OpenHarmony应用的包名、数字证书信息、描述应用允许申请的证书权限列表，以及允许应用调试的设备列表（如果应用类型为Release类型，则设备列表为空）等内容，每个应用包中均必须包含一个Profile文件。

进入Sdk\toolchains\lib目录下，打开命令行工具，执行如下命令。

```
java -jar provisionsigtool.jar sign --in UnsgnedReleasedProfileTemplate.json --out myApplication_ohos_Provision.p7b --keystore OpenHarmony.p12 --storepass 123456 --alias "OpenHarmony Application Profile Release" --sigAlg SHA256withECDSA --cert OpenHarmonyProfileRelease.pem --validity 365 --developer-id ohosdeveloper --bundle-name 包名 --permission 受限权限名（可选） --permission 受限权限名（可选） --distribution-certificate myApplication_ohos.cer
```

关于该命令的参数说明如下：

- provisionsigtool：Profile文件生成工具，文件在OpenHarmony SDK的Sdk\toolchains\lib路径下。

- in：Profile模板文件所在路径，文件在OpenHarmony SDK中Sdk\toolchains\lib路径下，该参数不能修改。

- out：输出的Profile文件名和路径。

- keystore：签发证书的密钥库路径，OpenHarmony密钥库文件名为OpenHarmony.p12，文件在OpenHarmony SDK中Sdk\toolchains\lib路径下，该参数不能修改。

- storepass：密钥库密码，密码为123456，该参数不能修改。

- alias：用于签名Profile私钥别名，OpenHarmony社区CA私钥存于OpenHarmony.p12密钥库文件中，该参数不能修改。

- sigalg：证书签名算法，该参数不能修改。

- cert：签名Profile的证书文件路径，文件在OpenHarmony SDK中Sdk\toolchains\lib路径下，该参数不能修改。

- validity：证书有效期，自定义天数。

- developer-id：开发者标识符，自定义一个字符串。

- bundle-name：填写应用包名。

- permission：可选字段，如果不需要，则可以不用填写此字段；如果需要添加多个受限权限，则如示例所示重复输入。受限权限列表如下：ohos.permission.READ_CONTACTS、ohos.permission.WRITE_CONTACTS。

- distribution-certificate：生成应用证书文件中生成的证书文件。

###### 7.4.4、配置应用签名信息

在真机设备上调试前，需要使用到制作的私钥（.p12）文件、证书（.cer）文件和Profile（.p7b）文件对调试的模块进行签名。

打开File > Project Structure，点击Project > Signing Configs > debug窗口中，去除勾选“Automatically generate signing”，然后配置指定模块的调试签名信息。

- Store File：选择密钥库文件，文件后缀为.p12，该文件为生成密钥和证书请求文件中生成的.p12文件。

- Store Password：输入密钥库密码，该密码为生成密钥和证书请求文件中填写的密钥库密码保持一致。

- Key Alias：输入密钥的别名信息，与生成密钥和证书请求文件中填写的别名保持一致。

- Key Password：输入密钥的密码，与Store Password保持一致。

- Sign Alg：签名算法，固定为SHA256withECDSA。

- Profile File：选择生成应用Profile文件中生成的Profile文件，文件后缀为.p7b。

- Certpath File：选择生成应用证书文件中生成的数字证书文件，文件后缀为.cer。

&nbsp;![18](./resource/18.jpg)

设置完签名信息后，点击OK进行保存，然后可以在工程下的build.gradle中查看签名的配置信息。

&nbsp;![19](./resource/19.jpg)

### 7.5、安装应用

###### 7.5.1、生成hap包

- 在主菜单栏点击Build > Build Hap(s)/App(s)。

&nbsp;![20](./resource/20.jpg)

- Hap生成路径。

&nbsp;![21](./resource/21.jpg)

###### 7.5.2、安装应用到Hi3516设备上

- 通过命令行安装

打开OpenHarmony SDK路径 \toolchains 文件夹下，执行如下hdc_std命令，其中path为hap的绝对路径。

```
hdc_std install -r path\entry-debug-standard-ark-signed.hap

```

举例，有如下信息，即表明安装成功

&nbsp;![22](./resource/22.jpg)

- 通过IDE安装

最新版IDE的新特性，DeVEco Studio 右上方，选择Hi3516设备，然后点击右侧的小三角形安装

&nbsp;![23](./resource/23.png)

### 7.6、查看日志

打开OpenHarmony SDK路径 \toolchains 文件夹，执行命令

```
hdc_std shell
#
#
# hilog | grep 需要筛选的日志
```