#  RK3568开发板标准设备应用开发HelloWorld

## 1、简介

#### 1.1.效果

&nbsp;![1](./resource/1.jpg)

#### 1.2.支持OpenHarmony版本

OpenHarmony 3.0 LTS、OpenHarmony 3.1 Beta。


#### 1.3.支持开发板

润和大禹系列HH-SCDAYU200开发板套件（OpenHarmony 3.1 Beta，该开发板无3.0 LTS版本）



## 2、 准备编译环境

搭建开发环境有windows和Ubuntu两种，这里以搭建Ubuntu环境(Docker方式)为例

#### 获取Docker环境

```
#安装Docker
cd ~
curl -fsSL https://get.docker.com -o get-docker.sh
chmod a+x get-docker.sh
sudo ./get-docker.sh

#获取Docker镜像
sudo docker pull swr.cn-south-1.myhuaweicloud.com/openharmony-docker/openharmony-docker-standard:0.0.5

#进入源码根目录执行如下命令，从而进入Docker构建环境
sudo docker run -it -v $(pwd):/home/openharmony swr.cn-south-1.myhuaweicloud.com/openharmony-docker/openharmony-docker-standard:0.0.5
```


## 3、 准备源码

#### 3.1.配置git和安装repo

参考[官网文章](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.0-LTS/zh-cn/device-dev/quick-start/quickstart-standard-docker-environment.md) 步骤学习

```
#安装Ubuntu 20.04

# 安装repo
curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 | sudo tee /usr/local/bin/repo >/dev/null
sudo chmod a+x /usr/local/bin/repo

# 设置git信息
git config --global user.name "yourname"
git config --global user.email "your-email-address"
git config --global credential.helper store

#安装git-lfs
sudo apt-get install git-lfs
```

#### 3.2.拉取源码

```
cd ~
mkdir openharmony
cd openharmony
repo init -u https://gitee.com/openharmony/manifest.git -b refs/tags/OpenHarmony-v3.1-Beta --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```
#### 3.3.执行prebuilts

```
#在源码根目录下执行脚本，安装编译器及二进制工具
#下载的prebuilts二进制默认存放在与OpenHarmony同目录下的OpenHarmony_2.0_canary_prebuilts下
./build/prebuilts_download.sh
```

## 4、 源码编译

```
#进入源码根目录，执行如下命令进行版本编译
./build.sh --product-name rk3568

#编译完成后会有如下显示
=====build rk3568 successful.
```

生成固件

&nbsp;![2_1](./resource/2_1.jpg)

## 5、镜像烧录

#### 5.1、安装USB驱动

下载USB驱动，[下载地址](https://www.t-firefly.com/doc/download/103.html)

双机DriverAssitant\ DriverInstall.exe*打开安装程序，点击下图所示的“驱动安装”按钮，按提示安装USB驱动：

&nbsp;![2](./resource/2.png)

说明：如果已经安装旧版本的烧写工具，请先点击"驱动卸载"按钮下载驱动，然后再点击"驱动安装"按钮安装驱动。


#### 5.2、安装烧录工具

下载烧录工具RKDevTool_Release，[下载地址](https://t.rock-chips.com/wiki.php?filename=%E8%B5%84%E6%96%99%E4%B8%8B%E8%BD%BD/%E8%B5%84%E6%96%99%E4%B8%8B%E8%BD%BD)

&nbsp;![3](./resource/3.png)

&nbsp;![4](./resource/4.png)

打开RKDevTool.exe

&nbsp;![5](./resource/5.png)

#### 5.3、修改配置
###### 5.3.1、将编译好的固件从ubuntu中拷贝到windows，通过单机右键，可以增加或者删除选项

&nbsp;![6](./resource/6.jpg)

###### 5.3.2、点击①这一列可以选择添加文件，添加的文件与名字一列修改成如上图所示

###### 5.3.3、使用USB线连接PC与开发板，然后长按recovery按键不放，给开发板上电，上电1-2秒之后松开按键，此时如下图所示

&nbsp;![7](./resource/7.jpg)

###### 5.3.4、如果如下图所示没有发现设备，说明没有进入download模式，1.可能没有安装驱动，重新安装驱动；2.USB口接触不良，换板子上另外一个USB接口

&nbsp;![8](./resource/8.png)

###### 5.3.5、点击设备分区表，注意，从Paramater以下，名字一列中，名字要与对应路径中文件名相同，否则会读取失败，如下图所示

&nbsp;![9](./resource/9.jpg)

###### 5.3.6、确定后，点击执行，等待烧录完成，如下图

&nbsp;![10](./resource/10.jpg)

#### 5.4、串口调试

###### 5.4.1、将串口线连接PC与开发板，打开securtCRT串口工具，在设备管理器查看端口

&nbsp;![11](./resource/11.png)

###### 5.4.2、串口波特率配置为1500000,如下图所示

&nbsp;![12](./resource/12.png)

## 6、镜像运行

按照上述步骤烧录，不需要执行镜像运行

## 7、创建应用

### 7.1、下载DevEco Studio [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta)

### 7.2、配置SDK，参考[配置OpenHarmony-SDK](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md)

### 7.3、创建应用

###### 7.3.1、选择模板
打开DevEco Studio，创建一个新工程，在DevEco Studio中点击File -> New Project，选择模板[Standard]Empty Ability。

&nbsp;![13](./resource/13.jpg)

###### 7.32、工程配置

进入配置工程界面，Project Type选择Application，Development mode选择Traditional coding,Language 选择eTS语言，其他参数根据实际需要设置即可，最后点击Finish即创建成功。

&nbsp;![13_1](./resource/13_1.jpg)

### 7.4、配置签名信息
说明：使用真机设备运行和调试OpenHarmony应用前，需要对应用进行签名才能正常运行

###### 7.4.1、生成密钥和证书请求文件

- 1.在主菜单栏点击Build > Generate Key and CSR。

- 2.在Key Store File中，可以点击Choose Existing选择已有的密钥库文件（存储有密钥的.p12文件）；如果没有密钥库文件，点击New进行创建。下面以新创建密钥库文件为例进行说明。

&nbsp;![14](./resource/14.jpg)

- 3.在Create Key Store窗口中，填写密钥库信息后，点击OK。

  Key Store File：选择密钥库文件存储路径。

  Password：设置密钥库密码，必须由大写字母、小写字母、数字和特殊符号中的两种以上字符的组合，长度至少为8位。请记住该密码，后续签名配置需要使用。

  Confirm Password：再次输入密钥库密码。

&nbsp;![15](./resource/15.jpg)

- 4.在Generate Key界面中，继续填写密钥信息后，点击Next。

  Alias：密钥的别名信息，用于标识密钥名称。请记住该别名，后续签名配置需要使用。

  Password：密钥对应的密码，与密钥库密码保持一致，无需手动输入。

  Validity：证书有效期，建议设置为25年及以上，覆盖应用/服务的完整生命周期。

  Certificate：输入证书基本信息，如组织、城市或地区、国家码等。

&nbsp;![16](./resource/16.jpg)

- 5.在Generate CSR界面，选择密钥和设置CSR文件存储路径，然后点击ok,创建CSR文件成功。

&nbsp;![17](./resource/17.jpg)

###### 7.4.2、生成应用证书文件

进入DevEco Studio安装目录的Sdk\toolchains\lib文件夹下（该SDK目录只能是OpenHarmony SDK，配置方法可参考配置OpenHarmony SDK），打开命令行工具，执行如下命令（如果keytool命令不能执行，请在系统环境变量中添加JDK的环境变量）。其中，只需要修改输入和输出即可快速生成证书文件，即修改**-infile指定证书请求文件csr文件路径，-outfile**指定输出证书文件名及路径。

```
keytool -gencert -alias "OpenHarmony Application CA" -infile myApplication_ohos.csr -outfile myApplication_ohos.cer -keystore OpenHarmony.p12 -sigalg SHA384withECDSA -storepass 123456 -ext KeyUsage:"critical=digitalSignature" -validity  3650 -rfc
```

关于该命令的参数说明如下：

- alias：用于签发证书的CA私钥别名，OpenHarmony社区CA私钥存于OpenHarmony.p12密钥库文件中，该参数不能修改。

- infile：证书请求（CSR）文件的路径。

- outfile：输出证书链文件名及路径。

- keystore：签发证书的CA密钥库路径，OpenHarmony密钥库文件名为OpenHarmony.p12，文件在OpenHarmony SDK中Sdk\toolchains\lib路径下，该参数不能修改。请注意，该OpenHarmony.p12文件并不是生成密钥和证书请求文件中生成的.p12文件。

- sigalg：证书签名算法，该参数不能修改。

- storepass：密钥库密码，密码为123456，该参数不能修改。

- ext：证书扩展项，该参数不能修改。

- validity：证书有效期，自定义天数。

- rfc：输出文件格式指定，该参数不能修改。

###### 7.4.3、生成应用Profile文件

Profile文件包含OpenHarmony应用的包名、数字证书信息、描述应用允许申请的证书权限列表，以及允许应用调试的设备列表（如果应用类型为Release类型，则设备列表为空）等内容，每个应用包中均必须包含一个Profile文件。

进入Sdk\toolchains\lib目录下，打开命令行工具，执行如下命令。

```
java -jar provisionsigtool.jar sign --in UnsgnedReleasedProfileTemplate.json --out myApplication_ohos_Provision.p7b --keystore OpenHarmony.p12 --storepass 123456 --alias "OpenHarmony Application Profile Release" --sigAlg SHA256withECDSA --cert OpenHarmonyProfileRelease.pem --validity 365 --developer-id ohosdeveloper --bundle-name 包名 --permission 受限权限名（可选） --permission 受限权限名（可选） --distribution-certificate myApplication_ohos.cer
```

关于该命令的参数说明如下：

- provisionsigtool：Profile文件生成工具，文件在OpenHarmony SDK的Sdk\toolchains\lib路径下。

- in：Profile模板文件所在路径，文件在OpenHarmony SDK中Sdk\toolchains\lib路径下，该参数不能修改。

- out：输出的Profile文件名和路径。

- keystore：签发证书的密钥库路径，OpenHarmony密钥库文件名为OpenHarmony.p12，文件在OpenHarmony SDK中Sdk\toolchains\lib路径下，该参数不能修改。

- storepass：密钥库密码，密码为123456，该参数不能修改。

- alias：用于签名Profile私钥别名，OpenHarmony社区CA私钥存于OpenHarmony.p12密钥库文件中，该参数不能修改。

- sigalg：证书签名算法，该参数不能修改。

- cert：签名Profile的证书文件路径，文件在OpenHarmony SDK中Sdk\toolchains\lib路径下，该参数不能修改。

- validity：证书有效期，自定义天数。

- developer-id：开发者标识符，自定义一个字符串。

- bundle-name：填写应用包名。

- permission：可选字段，如果不需要，则可以不用填写此字段；如果需要添加多个受限权限，则如示例所示重复输入。受限权限列表如下：ohos.permission.READ_CONTACTS、ohos.permission.WRITE_CONTACTS。

- distribution-certificate：生成应用证书文件中生成的证书文件。

###### 7.4.4、配置应用签名信息

在真机设备上调试前，需要使用到制作的私钥（.p12）文件、证书（.cer）文件和Profile（.p7b）文件对调试的模块进行签名。

打开File > Project Structure，点击Project > Signing Configs > debug窗口中，去除勾选“Automatically generate signing”，然后配置指定模块的调试签名信息。

- Store File：选择密钥库文件，文件后缀为.p12，该文件为生成密钥和证书请求文件中生成的.p12文件。

- Store Password：输入密钥库密码，该密码为生成密钥和证书请求文件中填写的密钥库密码保持一致。

- Key Alias：输入密钥的别名信息，与生成密钥和证书请求文件中填写的别名保持一致。

- Key Password：输入密钥的密码，与Store Password保持一致。

- Sign Alg：签名算法，固定为SHA256withECDSA。

- Profile File：选择生成应用Profile文件中生成的Profile文件，文件后缀为.p7b。

- Certpath File：选择生成应用证书文件中生成的数字证书文件，文件后缀为.cer。

&nbsp;![18](./resource/18.jpg)

设置完签名信息后，点击OK进行保存，然后可以在工程下的build.gradle中查看签名的配置信息。

&nbsp;![19](./resource/19.jpg)

### 7.5、安装应用

###### 7.5.1、生成hap包

- 在主菜单栏点击Build > Build Hap(s)/App(s)。

&nbsp;![20](./resource/20.jpg)

- Hap生成路径。

&nbsp;![21](./resource/21.jpg)

###### 7.5.2、安装应用到RK3568设备上

- 通过命令行安装

打开OpenHarmony SDK路径 \toolchains 文件夹下，执行如下hdc_std命令，其中path为hap的绝对路径。

```
hdc_std install -r path\entry-debug-standard-ark-signed.hap

```

举例，有如下信息，即表明安装成功

&nbsp;![22](./resource/22.jpg)

- 通过IDE安装

最新版IDE的新特性，DeVEco Studio 右上方，选择RK3568设备，然后点击右侧的小三角形安装

&nbsp;![23](./resource/23.png)

### 7.6、查看日志

打开OpenHarmony SDK路径 \toolchains 文件夹，执行命令

```
hdc_std shell
#
#
# hilog | grep 需要筛选的日志
```