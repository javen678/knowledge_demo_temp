## knowledge_demo_temp

#### 介绍
​    临时存放设备和应用样例。

#### FA

[应用demo列表](FA/README.md)

#### 设备端 demo

[设备端 demo列表](dev/docs/README.md)

#### Hi3516DV300 HelloWorld

[Hi3516 HelloWorld](docs/hi3516_dv300_fa_helloworld/README.md)

#### RK3568 HelloWorld

[RK3568 HelloWorld](docs/rk3568_fa_helloworld/README.md)

直播课程

[直播课程列表](./live/readme.md)

#### 目录结构
本仓库目录结构由如下4部分内容构成：

```
.
├── dev
│   ├── device // 存放相关设备开发sdk
│   ├── docs   // 存放dev 开发相关的智能设备开发文档
│   ├── team_x // 具体相关智能设备的应用代码
│   └── third_party // 设备开发相关的第三方库文件
├── FA
│   └── Entertainment  // 娱乐场景应用
│   │   ├── BombGame // 分布式传炸弹小游戏(ETS)
│   │   ├── DistrubutedMusicPlayer // 分布式音乐播放器(ETS)
│   │   ├── TicTacToeGame // 分布式井子过三关游戏(ETS)
│   └── Shopping  // 购物场景应用
│   │   ├── MyAccountBook // 家庭账本
│   │   ├── DistributedShoppingCart // 分布式购物车
│   │   ├── DistributedOrder // 分布式菜单

```



#### 快速上手体验

可以按如下步骤，快速体验HI3861开发版：
    1、按照[官方网站](https://gitee.com/openharmony?_from=gitee_search)指引，搭建开发环境；
    2、[官方网站](https://gitee.com/openharmony?_from=gitee_search)下载源码；
    3、下载本仓库的demo源码，并按照要求配置修改源码
   4、编译、烧写；
   5、体验demo的效果。

#### 参与贡献

1. Fork 本仓库

2. 新建 Feat_xxx 分支

3. 提交代码

4. 新建 Pull Request

   具体步骤细节请参考[代码贡献文档]()


#### FAQs

​    待补充

#### 相关仓

[知识体系相关文档仓库](https://gitee.com/openharmony-sig/knowledge)

