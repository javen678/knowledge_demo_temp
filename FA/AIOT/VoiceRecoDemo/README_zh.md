# VoiceRecoDemo应用代码介绍

### 简介
    
VoiceRecoDemo是装在OpenHarmony系统中的应用，样例可以使用在汽车4S店的展示厅，客户可以通过语音来控制显示具体车辆的内饰，外观，车灯，后备箱等。样例利用了OpenHarmony的底层音频采集功能，获取到录音数据，将数据通过科大讯飞的接口进行语音识别。这个样例在rk3566板子上运行，由于设备没有mic，先使用录制好的pcm文件进行模拟音频采集，进行语音识别。


### 样例效果

<video id="video" controls="" preload="none">
    <source id="mp4" src="1.mp4" type="video/mp4">
</video>

### 代码结构

本demo包括entry模块
![](1.png)



### 安装部署

##### 1.代码编译运行步骤

1）下载此项目，[链接](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/FA/AI/VoiceRecoDemo)。

2）开发环境搭建，开发工具：DevEco Studio 3.0 Beta1，SDK 请配置请参考[配置OpenHarmony SDK](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md) 。

3）导入OpenHarmony工程：OpenHarmony应用开发，只能通过导入Sample工程的方式来创建一个新工程，具体可参考[导入Sample工程创建一个新工程](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/import-sample-to-create-project.md)

4）OpenHarmony应用运行在真机设备上，需要对应用进行签名，请参考[OpenHarmony应用签名](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

### 约束限制

1. 提前准好已实名认证的开发者联盟账号 ，具体[参考](https://developer.huawei.com/consumer/cn/)
2. 更多应用编译和签名资料，请[参考](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/deveco-studio-overview.md#/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md)
3. 更多开发资料请登录应用开发官网： [参考](https://developer.harmonyos.com/cn/)
4. 系统需要支持WebSocket的api，如果不支持需要自己添加对应的napi接口。
5. 如果设备没有mic，模拟使用audio目录下的pcm文件，将这些文件放置到设备的/data/audio目录下，代码中模拟的内饰，外观，引擎和尾部是通过读取/data/audio对应的pcm文件来获取语音数据的

