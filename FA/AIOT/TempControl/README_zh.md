# TempControl应用代码介绍

### 简介

TempControl是装在手机端的应用，根据温度传感设备上报的数据来显示设备所在环境中的温度，并在手机端实时展现出来；

### 代码结构

本demo包括entry模块
![](1.png)



### 安装部署

##### 1.代码编译运行步骤

1）下载此项目，[链接](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/FA/TempControl )。

2）开发环境搭建，开发工具：DevEco Studio 3.0 Beta1，SDK 请配置请参考[配置OpenHarmony SDK](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md) 。

3）导入OpenHarmony工程：OpenHarmony应用开发，只能通过导入Sample工程的方式来创建一个新工程，具体可参考[导入Sample工程创建一个新工程](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/import-sample-to-create-project.md)

4）OpenHarmony应用运行在真机设备上，需要对应用进行签名，请参考[OpenHarmony 应用签名](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

### 约束限制

1. 提前准好已实名认证的开发者联盟账号 ，具体[参考](https://developer.huawei.com/consumer/cn/)
2. 更多应用编译和签名资料，请[参考](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/deveco-studio-overview.md#/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md)
3. 更多开发资料请登录应用开发官网： https://developer.harmonyos.com/cn/
