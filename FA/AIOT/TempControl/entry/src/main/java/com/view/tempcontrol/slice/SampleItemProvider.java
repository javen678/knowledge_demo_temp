/*
 * Copyright (c) 2021 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.view.tempcontrol.slice;

import com.view.tempcontrol.ResourceTable;
import com.view.tempcontrol.model.TempSampleItemasdf;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.List;

public class SampleItemProvider extends BaseItemProvider {

    private List<TempSampleItemasdf> list;
    private AbilitySlice slice;
    public SampleItemProvider(List<TempSampleItemasdf> list, AbilitySlice slice) {
        this.list = list;
        this.slice = slice;
    }
    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        if (list != null && position >= 0 && position < list.size()){
            return list.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
        final Component cpt;
        if (convertComponent == null) {
            cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_item_sample, null, false);
        } else {
            cpt = convertComponent;
        }
        TempSampleItemasdf tempSampleItemasdf = list.get(position);
        Text text = (Text) cpt.findComponentById(ResourceTable.Id_item_index);
        text.setText(tempSampleItemasdf.getTitle());

        DirectionalLayout itemLayout = (DirectionalLayout) cpt.findComponentById(ResourceTable.Id_item);
        itemLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                Intent intent1 = new Intent();
                intent1.setParam("user", "david");
                slice.present(new DetailAbilitySlice(), intent1);

                //TODO 后续该item的数据可以通过网络请求拿到数据，用户可以自定义的配置相关数据，动态展示

            }
        });
        return cpt;
    }
}
