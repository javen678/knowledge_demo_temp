/*
 * Copyright (c) 2021 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.view.tempcontrol.bean;

public class ServicesBean {
    private String service_id;
    private PropertiesBean properties;
    private String event_time;
    public void setService_id(String service_id) {
        this.service_id = service_id;
    }
    public String getService_id() {
        return service_id;
    }

    public void setProperties(PropertiesBean properties) {
        this.properties = properties;
    }
    public PropertiesBean getProperties() {
        return properties;
    }

    public void setEvent_time(String event_time) {
        this.event_time = event_time;
    }
    public String getEvent_time() {
        return event_time;
    }
}
