# OpenHarmony分布式菜单

## 一、简介

#### 1.样例效果

分布式菜单demo 模拟的是多人聚餐点菜的场景，不需要扫码关注公众号等一系列操作，通过分布式数据库可以方便每个人可及时查看到订单详情，数量，总额等；效果如下：

- demo效果（润和HiSpark Taurus AI Camera(Hi3516d）

![show](resources/3516.gif)



- demo效果（HH-SCDAYU200）

![show](resources/3568.gif)

#### 2.设计OpenHarmony技术特性

- ETS UI 
- 分布式调度
- 分布式数据管理

####  3.支持OpenHarmony版本

OpenHarmony 3.0 LTS 、OpenHarmony 3.1 Beta

#### 4.支持开发板

- 润和HiSpark Taurus AI Camera(Hi3516d)开发板套件（OpenHarmony 3.0 LTS 、OpenHarmony 3.1 Beta）
- 润和大禹系列HH-SCDAYU200开发板套件（OpenHarmony 3.1 Beta，该开发板无3.0 LTS版本）

## 二、快速上手

#### 1.标准设备环境准备

以润和HiSpark Taurus AI Camera(Hi3516d)开发板套件为例

- [获取OpenHarmony源码](https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/OpenHarmony-v3.1-beta.md)OpenHarmony 3.1 Beta；
- [源码编译](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-standard-running-rk3568-build.md)
- [开发板烧录](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-standard-running-hi3516-burn.md)

以润和大禹系列HH-SCDAYU200开发板套件为例

-  [开发板上新 | RK3568开发板上丝滑体验OpenHarmony标准系统](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/tree/master/dev/docs/rk3568_quick_start)

#### 2.应用编译环境准备

- 下载DevEco Studio版本 [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta)；
- 配置SDK， [配置OpenHarmony-SDK](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md)
- DevEco Studio 点击File -> Open 导入本下面的代码工程DistributedOrder

#### 3.项目下载和导入

1）git下载

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_temp.git
```

2）项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/Shopping/DistributedOrder


#### 4.安装应用

- [配置应用签名信息](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

- [安装应用](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/installing-openharmony-app.md)

  体验分布式菜单时，需要两个开发板，连接同一个wifi或使用网线连接并配置同一网段IP地址

  ```
  hdc_std shell ifconfig eth0 192.168.1.111 netmask 255.255.255.0  hdc_std shell ifconfig eth0 192.168.1.222 netmask 255.255.255.0
  ```

**PS**环境准备，源码下载，编译，烧录设备，应用部署的完整步骤请参考[这里](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/deveco-studio-overview.md#/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

##  三、关键代码解读

#### 1.目录结构

```
├─entry
│  └─src
│      └─main
│          │  config.json  // 应用配置文件
│          │  
│          ├─ets
│          │  └─MainAbility
│          │      │  app.ets  // 应用程序主入口
│          │      │  
│          │      ├─model
│          │      │      CommonLog.ets  // 日志类
│          │      │      MenuData.ets  // 初始化菜单数据类
│          │      │      MenuListDistributedData.ets  // 加入菜单分布式数据库
│          │      │      RemoteDeviceManager.ets  // 分布式拉起设备管理类
│          │      │      SubmitData.ets   // 结算订单分布式数据库
│          │      │      
│          │      └─pages
│          │              detailedPage.ets // 菜品详细页面
│          │              index.ets // 首页
│          │              menuAccount.ets // 订单详情页面
│          │              
│          └─resources
│              ├─base
│              │  ├─element
│              │  │      string.json
│              │  │      
│              │  ├─graphic
│              │  ├─layout
│              │  ├─media   // 存放媒体资源
│              │  │      icon.png
│              │  │      icon_add.png
│              │  │      icon_back.png
│              │  │      icon_cart.png
│              │  │      
│              │  └─profile
│              └─rawfile
```

#### 2.日志查看方法

```
hdc_std shell
hilog | grep ordering 
```

#### 3.关键代码

- UI界面，设备流转：index.ets
- 设备管理：RemoteDeviceManager.ets
- 分布式数据管理：MenuListDistributedData.ets  SubmitData.ets

## 四、如何从零开发分布式菜单

[从零开发分布式菜单](quick_develop.md)

##  五、参考链接

- [OpenHarmony基于TS扩展的声明式开发范式](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-ts/Readme-CN.md)
- [OpenHarmony应用接口](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis/Readme-CN.md)
- [OpenHarmonyETS开发FAQ](https://gitee.com/Cruise2019/team_x/blob/master/homework/ets_quick_start/ETS%E5%BC%80%E5%8F%91FAQ.md)



