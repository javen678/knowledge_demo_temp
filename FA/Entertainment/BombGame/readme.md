# OpenHarmony 分布式传炸弹小游戏

## 一、简介

#### 1.样例效果

本Demo是基于OpenHarmony3.1 Beta，使用ETS语言编写的应用。该样例使用eTS编写，在邀请用户进行设备认证后，用户根据操作提示通过分布式流转实现随机传递炸弹的效果。

邀请用户（Hi3516d）

![show](resources/3516_1.gif)

开始游戏（Hi3516d）

![show](resources/3516_2.gif)

开始游戏（HH-SCDAYU200）

![show](resources/3568_start_game.gif)

#### 2.涉及OpenHarmony技术特性

- eTS UI
- 分布式调度

####  3.支持OpenHarmony版本

OpenHarmony 3.0 LTS、OpenHarmony 3.1 Beta。

#### 4.支持开发板

- 润和HiSpark Taurus AI Camera(Hi3516d)开发板套件（OpenHarmony 3.0 LTS、OpenHarmony 3.1 Beta）
- 润和大禹系列HH-SCDAYU200开发板套件（OpenHarmony 3.1 Beta，该开发板无3.0 LTS版本）

## 二、快速上手

#### 1.标准设备环境准备

润和HiSpark Taurus AI Camera(Hi3516d)开发板套件:

- [Hi3516DV300开发板标准设备HelloWorld](../../../docs/hi3516_dv300_fa_helloworld/README.md)，参考环境准备、编译和烧录章节;

润和大禹系列HH-SCDAYU200开发套件：

- [开发板上新 | RK3568开发板上丝滑体验OpenHarmony标准系统](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/tree/master/dev/docs/rk3568_quick_start);

#### 2.应用编译环境准备

- 下载DevEco Studio [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta)；
- 配置SDK，参考 [配置OpenHarmony-SDK](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md);
- DevEco Studio 点击File -> Open 导入本下面的代码工程BombGame;

#### 3.项目下载和导入

1）git下载

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_temp.git
```

2）项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/Entertainment/BombGame


#### 4.安装应用

- [配置应用签名信息](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

- 安装应用

  打开**OpenHarmony SDK路径 \toolchains** 文件夹下，执行如下hdc_std命令，其中**path**为hap包所在绝对路径。

  ```text
  hdc_std install -r path\entry-debug-standard-ark-signed.hap
  ```

**PS** 分布式流转流转时，需要多个开发板，连接同一个wifi或使用网线连接


##  三、关键代码解读

#### 1.目录结构

```
.
├─entry\src\main
│     │  config.json // 应用配置
│     ├─ets
│     │  └─MainAbility
│     │      │  app.ets //ets应用程序主入口
│     │      └─pages
│     │              CommonLog.ets // 日志类
│     │              game.ets // 游戏首页
│     │              RemoteDeviceManager.ets // 设备管理类
│     └─resources // 静态资源目录
│         ├─base
│         │  ├─element
│         │  ├─graphic
│         │  ├─layout
│         │  ├─media // 存放媒体资源
│         │  └─profile
│         └─rawfile
```

#### 2.日志查看方法

```
hdc_std shell
hilog | grep BombGame 
```

#### 3.关键代码

- UI界面，设备流转：game.ets
- 设备管理：RemoteDeviceManager.ets

## 四、如何从零开发传炸弹

[从零开发传炸弹小游戏](quick_develop.md)

##  五、参考链接

- [OpenHarmony基于TS扩展的声明式开发范式](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-ts/Readme-CN.md)
- [OpenHarmony应用接口](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis/Readme-CN.md)
- [OpenHarmonyETS开发FAQ](https://gitee.com/Cruise2019/team_x/blob/master/homework/ets_quick_start/ETS%E5%BC%80%E5%8F%91FAQ.md)
- [开发板上新 | RK3568开发板上丝滑体验OpenHarmony标准系统](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/tree/master/dev/docs/rk3568_quick_start)

