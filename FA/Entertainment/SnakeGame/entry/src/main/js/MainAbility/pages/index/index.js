/*
 * Copyright (c) 2021 ArleneLin
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


export default {
    data: {
        title: "",
        snakeSize: 20,      // 蛇身格子像素大小
        w: 700,             // 背景的宽度
        h: 200,             // 背景的高度
        score: 0,           // 得分为0
        snake : [],         // 数组用来存蛇每个格子的位置
        ctx: null,          // 用来调用填充颜色的
        food: null,         // 食物位置
        direction: '',      // 按键的状态
        gameOver: false,    // 游戏状态
        tail: {             // 记录更新后蛇头的位置
            x: 0,
            y: 0
        },
        interval: null     // 获得setInterval()的返回值
    },
    onInit() {
        this.title = this.$t('strings.world');
    },
    onShow() {
        // 通过$refs得到组件，进而调用组件的变量和方法
        const canvas = this.$refs.canvasref;
        // 指定了二维绘画
        this.ctx = canvas.getContext("2d");
        // 第一次打开app时，初始化蛇的方向
        this.direction = 'down';
        // 调用初始化蛇体的方法
        this.drawSnake()
        // 创建食物的位置
        this.createFood()
        // 渲染帧画面
        this.paint()
    },
    // 画背景
    drawArea() {
        var ctx = this.ctx
        // 设置填充颜色的
        ctx.fillStyle = '#6699CC';
        // 填充
        ctx.fillRect(0, 0, this.w, this.h);
        // 设置矩阵颜色的
        ctx.strokeStyle = '#00000';
        // 矩阵的线宽
        ctx.lineWidth = 5;
        // 绘制矩阵（不填色的）
        ctx.strokeRect(0, 0, this.w, this.h);
        this.ctx = ctx
    },
    // 创建蛇体
    drawSnake() {
        var len = 6;
        var snake = [];
        // 默认蛇的长度为6
        for (var i = len - 1; i >= 0; i--) {
            // 将x轴和y轴的坐标数据存到数组中，这些数据就是每个蛇格子的位置
            snake.push({
                x: 0,
                y: i
            });
        }
        // 更新蛇的长度
        this.snake = snake;
    },
    // 设计蛇身的颜色的
    bodySnake(x, y) {
        //single square of snake
        var ctx = this.ctx;
        // 蛇的颜色及填充的位置和大小
        ctx.fillStyle = '#F4B9B2';
        // fillRect()指的是要填充的位置及大小 参数说明：fillRect(X轴位置, Y轴位置, 宽度, 高度）
        ctx.fillRect(x * this.snakeSize, y * this.snakeSize, this.snakeSize, this.snakeSize);
        // 蛇的内部格子边框颜色，加了才会分割
        ctx.strokeStyle = '#846267';
        ctx.strokeRect(x * this.snakeSize, y * this.snakeSize, this.snakeSize, this.snakeSize);
        this.ctx = ctx;
    },
    // 设计食物的颜色的
    cookie(x, y) {
        var ctx = this.ctx;
        // 食物的颜色及填充位置和大小
        ctx.fillStyle = '#FA7E61';
        ctx.fillRect(x * this.snakeSize, y * this.snakeSize, this.snakeSize, this.snakeSize);
        this.ctx = ctx;
    },
    // 创建食物的位置
    createFood() {
        // 随机生成食物的位置
        // 这里的10是背景高度（宽度）/ 格子高度（宽度），即 200 / 20 = 10
        this.food = {
            x: Math.floor((Math.random() * 35) + 1),
            y: Math.floor((Math.random() * 10) + 1)
        }
        for (var i = 0; i < this.snake.length; i++) {
            // 获取刚创建蛇的时候，蛇上每个点的位置，再和食物的位置进行比较
            var snakeX = this.snake[i].x;
            var snakeY = this.snake[i].y;
            // 如果食物的位置出现在蛇的身上，则重新生成
            if (this.food.x === snakeX && this.food.y === snakeY || this.food.y === snakeY && this.food.x === snakeX) {
                this.food.x = Math.floor((Math.random() * 35) + 1);
                this.food.y = Math.floor((Math.random() * 10) + 1);
            }
        }
    },
    // 检查是否碰壁
    checkCollision(x, y, array) {
        for (var i = 0; i < array.length; i++) {
            if(array[i].x === x && array[i].y === y){
           	 return true;
	}
        }
        return false;
    },
    // 鼠标点击绑定的事件
    onStartGame(direct){
        // 设置游戏初始状态，控制text标签的显示
        this.gameOver = false;
        // 通过对应的参数，获取对应direct的字段
        if (direct == 1) {
            this.direction = 'up';
        } else if (direct == 2) {
            this.direction = 'left';
        } else if (direct == 3) {
            this.direction = 'down';
        } else if (direct == 4) {
            this.direction = 'right';
        }
        // 调用绘图方法
        this.paint();
        // 设置蛇的移动间隔时间，也可以理解为绘图的时间间隔
        if (this.interval == null) {
            // setInterval() 方法可按照指定的周期（以毫秒计）来调用函数或计算表达式
            this.interval = setInterval(this.paint, 350);
        }
    },
    // 每次移动刷新的操作，即帧画面创建和渲染的流程
    paint() {
        // 调用画背景
        this.drawArea()
        // 获得蛇头的位置的初始坐标
        var snakeX = this.snake[0].x;
        var snakeY = this.snake[0].y;
        // 移动操作，更新数据
        if (this.direction == 'right') {
            snakeX++;
        }
        else if (this.direction == 'left') {
            snakeX--;
        }
        else if (this.direction == 'up') {
            snakeY--;
        } else if (this.direction == 'down') {
            snakeY++;
        }
        // 反向移动或碰撞壁的时候，游戏失败，重启游戏
        if (snakeX == -1 || snakeX == this.w / this.snakeSize || snakeY == -1 || snakeY == this.h / this.snakeSize || this.checkCollision(snakeX, snakeY, this.snake)) {
            //ctx.clearRect(0,0,this.w,this.h); //clean up the canvas
            clearInterval(this.interval);
            this.interval = null
            this.restart()
            return;
        }
        //  判断是否吃到食物
        if(snakeX == this.food.x && snakeY == this.food.y) {
            // 吃到食物
            // 将食物的位置记录下来
            this.tail = {x: snakeX, y: snakeY};
            // 分数加2
            this.score = this.score+2;
            // 再创建食物
            this.createFood();
        } else {
            // 没吃到食物
            // 去掉数组最后的元素并返回，相当于删除蛇尾
            this.tail = this.snake.pop();
            // 将移动更新后蛇头的位置加到tail中
            this.tail.x = snakeX;
            this.tail.y = snakeY;
        }
        // unshift()方法可向数组的开头添加一个或多个元素
        // 将更新后的节点添加蛇头
        this.snake.unshift(this.tail);
        // 渲染每个蛇身格子的位置
        for(var i = 0; i < this.snake.length; i++) {
            this.bodySnake(this.snake[i].x, this.snake[i].y);
        }
        // 渲染食物的位置
        this.cookie(this.food.x, this.food.y);
    },
    // 重启操作
    restart() {
        this.drawArea()
        this.drawSnake()
        this.createFood()
        this.gameOver = true
        this.score = 0
    },
}