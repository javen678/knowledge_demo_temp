# OpenHarmony上跑起ArkUI小游戏

 我们基于JS开发了一款如下视频所示贪吃蛇小游戏应用 。

https://gitee.com/ArleneLin/open-harmony-game/raw/1a5128060e0d100ff8cf9969439850ccd41ba389/%E8%B4%AA%E5%90%83%E8%9B%87/WeChat_20211224170645.mp4

打开应用，按下↓方向键，即可控制小蛇开始游戏。

完整的项目结构目录如下：

```
├─entry
│  └─src
│      └─main
│          │  config.json // 应用配置
│          │
│          ├─js
│          │  └─MainAbility
│          │      ├─common // 存放图片视频等资源
│          │      │
│		   │      ├─i18n // 语言包
│		   │	  │
│          │      └─pages
│		   │			└─index
│          │              	index.css  // 页面样式
│          │              	index.hml  // 游戏首页
│          │              	index.js  // 游戏逻辑
│          │     app.js // 应用生命周期
│		   │
│          └─resources // 静态资源目录
│              ├─base
│              │  ├─element
│              │  │
│              │  └─media // 存放媒体资源
│              └─rawfile
```



步骤如下：编写声明式UI界面、样式修改和编写游戏逻辑

### 一. 编写声明式UI界面

##### 1.1 新增工程

​	 在 DevEco Studio 中点击 File -> New Project -> Empty Ability->Next，Language 选择 JS 语言，最后点击 Finish 即创建成功。 

https://gitee.com/ArleneLin/open-harmony-game/raw/master/%E8%B4%AA%E5%90%83%E8%9B%87/1640331890532.png



##### 1.2 编写游戏页面

https://gitee.com/ArleneLin/open-harmony-game/raw/master/%E8%B4%AA%E5%90%83%E8%9B%87/1640331945548.png

效果图如上，可以分为三部分

- 贪吃蛇移动区域展示

  采用<canvas>组件描绘出贪吃蛇的移动区域

  ```
  <canvas ref="canvasref" style="width: 720px; height: 200px; background-color: black;margin-left: 10px;margin-right: 10px;"></canvas>
  ```

- 游戏按键

  通过放置四张上下左右按键图片来控制贪吃蛇的移动， 想要的效果是方向键如同键盘方向的布局，所以只需对下面三个按键进行处理。可以用一个div标签把它们包裹起来，再定义一个新属性 

  ```
  <div class="btn">
  <!--上按键-->
      <image src="/common/up.png" class="backBtnup" onclick="onStartGame(1)"></image>
  
  <!--下面三个按键用同一样式，所以用同一个div包围-->
      <div class="directsecond">
      <!--左按键-->
          <image src="/common/left.png" class="backBtnleft" onclick="onStartGame(2)"></image>
      <!--下按键-->
          <image src="/common/down.png" class="backBtncenter" onclick="onStartGame(3)"></image>
      <!--右按键-->
          <image src="/common/right.png" class="backBtnright" onclick="onStartGame(4)"></image>
      </div>
  </div>
  ```

- 游戏分数

  ```
  <!--用if判断，如果游戏结束，则显示该模块-->
      <text if="{{gameOver}}" class="scoretitle">
          <span>Game Over!!!</span>
      </text>
  <!--用if判断，如果游戏没有结束，则显示该模块。显示得分-->
      <text if="{{!gameOver}}" class="scoretitle">
          <span>Score: {{score}}</span>
      </text>
  ```



### 二. 样式修改

### 三. 编写游戏逻辑

##### 3.1 绑定点击事件

按钮的触发是通过点击屏幕，所以要有点击事件，鼠标点击事件是有对应的方法，通过方法传不同的参数来区别不同的方向

##### 3.2 食物的生成

随机生成，判断食物生成的位置如果出现在蛇身上，则重新生成。

##### 3.3 蛇身的初始化 （由于案例比较简单，所以没有设定随机生成初始位置）

给定长度并设定一个空数组，通过for循环，把x和y的坐标push进数组，作为蛇身每格的位置。

##### 3.4 蛇运动

移动是靠每帧重绘位置，吃到食物就头部立刻加长，没吃到食物就去掉尾部，把头部方向指向的下一个位置记录到数组头部，等下次刷新帧

##### 3.5 判定游戏结束

碰壁、相对方向移动、形成环路



### 四. 项目下载和导入

项目仓库地址：

https://gitee.com/ArleneLin/open-harmony-game.git

4.1 git下载

```
git clone https://gitee.com/ArleneLin/open-harmony-game.git
```

4.2 项目导入

打开 DevEco Studio，点击 File->Open->下载路径