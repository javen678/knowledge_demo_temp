# WarChess

## 一、简介

#### 1.样例效果
本Demo是基于OpenHarmony3.0 LTS，使用js语言编写的应用。该样例使用js编写，用户通过不同的触屏方式来实现对战棋游戏的游玩效果，支持运行在OpenHarmony3.0+版本标准系统上。

[注意]():该demo引用了部分来源于RPGMaker的图片素材。

![show](resources/demo_video.mp4)

#### 2涉及OH技术特性
- JS UI

#### 3.支持OH版本
OpenHarmony 3.0 LTS

## 二、快速上手

#### 1.标准设备环境准备

- [获取OpenHarmony源码](https://www.openharmony.cn/pages/0001000202/#%E5%AE%89%E8%A3%85%E5%BF%85%E8%A6%81%E7%9A%84%E5%BA%93%E5%92%8C%E5%B7%A5%E5%85%B7)，OpenHarmony版本须3.0LTS以上；
- [安装开发板环境](https://www.openharmony.cn/pages/0001000400/#hi3516%E5%B7%A5%E5%85%B7%E8%A6%81%E6%B1%82)
- [开发板烧录](https://www.openharmony.cn/pages/0001000401/#%E4%BD%BF%E7%94%A8%E7%BD%91%E5%8F%A3%E7%83%A7%E5%BD%95)

#### 2.应用编译环境准备

- 下载DevEco Studio 3.0.0.601及以上版本 [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta)；
- 配置SDK，参考 [配置OpenHarmony-SDK](https://www.openharmony.cn/pages/00090001/#%E5%89%8D%E6%8F%90%E6%9D%A1%E4%BB%B6)
- DevEco Studio 点击File -> Open 导入本下面的代码工程WarChess

#### 3.项目下载和导入

项目地址: https://gitee.com/cocollria/war-chess

1）git下载
```
git clone https://gitee.com/cocollria/war-chess.git
```

2）项目导入

打开DevEco Studio,点击File->Open->下载路径/war-chess/warchess

#### 4.安装应用

- [配置应用签名信息](https://www.openharmony.cn/pages/00090003/#%E7%94%9F%E6%88%90%E5%AF%86%E9%92%A5%E5%92%8C%E8%AF%81%E4%B9%A6%E8%AF%B7%E6%B1%82%E6%96%87%E4%BB%B6)

- 安装应用

  打开**OpenHarmony SDK路径 \toolchains** 文件夹下，执行如下hdc_std命令，其中**path**为hap包所在绝对路径。

  ```
  hdc_std install -r path\entry-debug-standard-ark-signed.hap
  ```

## 三、关键代码解读

#### 1.目录结构
```
.
├─entry\src\main
│     │  config.json // 应用配置
│     ├─js
│     │  └─MainAbility
│     │      ├─common
│     │      │    ├─  js //定义一些对象和方法
│     │      │    │    ├─Attack_Time.js
│     │      │    │    ├─Backstate_Map.js
│     │      │    │    └─Chess.js
│     │      │    └─picture // 存放要用的贴图
│     │      ├─i8n
│     │      ├─pages
│     │      │    ├─index
│     │      │    │    ├─index.css
│     │      │    │    ├─index.hml
│     │      │    │    └─index.js // 游戏首页
│     │      │    └─second
│     │      │         ├─second.css
│     │      │         ├─second.hml
│     │      │         └─second.js
│     │      └─app.js
│     └─resources // 静态资源目录
│         ├─base
│         │  ├─element
│         │  ├─media
│         └─rawfile
```

#### 2.关键代码
- UI界面：second.hml
- 游戏逻辑：second.js
- 寻路算法: Backstate_Map.js

## 四、如何开发战棋小游戏

[开发战棋小游戏](quick_develop.md)

## 五、参考链接

- [基于JS扩展的类Web开发范式](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-js/Readme-CN.md)

- [OpenHarmony应用接口](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis/Readme-CN.md)

