# 用JS写一个OpenHarmony拼图小游戏

## 一、简介

#### 1.样例效果

本Demo是基于OpenHarmony3.0 LTS，使用JS语言编写的拼图小游戏。

![游戏效果展示](resources/%E6%95%88%E6%9E%9C%E5%B1%95%E7%A4%BA.gif)

#### 2.涉及OH技术特性

- JS UI

####  3.支持OH版本

OpenHarmony 3.0 LTS


##  二、目录结构

```
.
├─entry\src\main
│     │  config.json // 应用配置
│     ├─js
│     │  └─MainAbility
│     │      │  app.js
│     │      ├─common
│     │      │       images.js // 图片列表
│     │      ├─i18n
│     │      └─pages
│     │              ├─index // 首页
│     │              │       index.css
│     │              │       index.hml
│     │              │       index.js
│     │              └─jigsaw // 游戏页面
│     │                      jigsaw.css
│     │                      jigsaw.hml
│     │                      jigsaw.js
│     └─resources // 静态资源目录
│         ├─base
│         │  ├─element
│         │  └─media // 存放媒体资源
│         └─rawfile
```


## 三、关键代码解读

- [代码解读](code_explain.md)