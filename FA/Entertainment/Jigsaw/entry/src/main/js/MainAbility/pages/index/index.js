import router from '@system.router'

export default {
    data: {
        title: "拼图小游戏",
        rule: "玩法介绍：参照左边的原图，在屏幕上下左右滑动，使右边的拼图恢复原状，觉得困难可以开启提示哦"
    },

    playgame(num) {
        router.replace({
            uri: "pages/jigsaw/jigsaw",
            params: {
                block: num,
            },
        })
    }
}
