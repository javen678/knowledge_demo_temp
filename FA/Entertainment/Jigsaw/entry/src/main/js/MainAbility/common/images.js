export let images =  [
    {
        index: 0,
        src: "common/0.png",
    },
    {
        index: 1,
        src: "common/1.png",
    },
    {
        index: 2,
        src: "common/2.png",
    },
    {
        index: 3,
        src: "common/3.png",
    },
    {
        index: 4,
        src: "common/4.png",
    },
    {
        index: 5,
        src: "common/5.png",
    },
    {
        index: 6,
        src: "common/6.png",
    },
    {
        index: 7,
        src: "common/7.png",
    },
    {
        index: 8,
        src: "common/8.png",
    },
    {
        index: 9,
        src: "common/9.png",
    },
    {
        index: 10,
        src: "common/10.png",
    },
    {
        index: 11,
        src: "common/11.png",
    },
    {
        index: 12,
        src: "common/12.png",
    },
    {
        index: 13,
        src: "common/13.png",
    },
    {
        index: 14,
        src: "common/14.png",
    },
]

export default images;