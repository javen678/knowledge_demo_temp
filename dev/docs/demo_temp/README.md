

# 温度上报设备端代码介绍

## 简介：
  	本Demo是基于LYEVK-3861A IoT 开发套件开发，通过温湿度传感器模块获取温度，并上传华为云服务器。
  	特别说明：设备端代码基于开源版本code_3.0 LTS版本

## 产品配置



## 代码编译

 1） 拉取相关代码后，拷贝代码demo_park目录到OpenHarmony的源码中。目录为applications/sample/wifi-iot/app

可见demo_temp目录结构如下：  
./
├── BUILD.gn
├── include
│   ├── aht20.h
│   ├── iot_adc.h
│   ├── iot_wifi.h
│   ├── oc_mqtt.h
│   ├── oc_mqtt_profile_package.h
│   └── rtc_base.h
├── README.md
└── src
    ├── aht20.c
    ├── demo_temp.c
    ├── iot_adc.c
    ├── iot_wifi.c
    ├── oc_mqtt.c
    ├── oc_mqtt_profile_package.c
    └── rtc_base.c

3） 修改app/BUILD.gn 文件

    配置./applications/sample/wifi-iot/app/BUILD.gn文件，在features字段中增加索引，使目标模块参与编译。features字段指定业务模块的路径和目标，features字段配置如下。
```  
    import("//build/lite/config/component/lite_component.gni")
    
    lite_component("app") {
    features = [
        "demo_temp",
    ]
    deps = []
}

```
4） 打开三方驱动中的编译宏
		打开：device/hisilicon/hispark_pegasus/sdk_liteos/build/config/usr_config.mk
         找到：CONFIG_I2C_SUPPORT is not set，修改为CONFIG_I2C_SUPPORT=y

5）代码修改

​      修改以下的代码参数为当前的wifi和密码。

```
BOARD_ConnectWifi("Test", "11111111");
```


6) 引入三方paho_mqtt库

 下载三方开源[paho_mqtt](https://gitee.com/flowerzhang/third_party_paho_mqtt)库，改名为paho_mqtt放置在OpenHarmony源码的third_party目录下。
 备注：此库已经做过移植适配，可直接使用。

7） 编译后烧录bin 文件
	    编译环境、源码获取等参考：https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/Readme-CN.md
   	 编译命令：1、hb set // 设置工程目录和选择相应目标 
   	 					   wifiiot_hispark_pegasus

​                   2、hb build -f 
   	 烧录步骤：请参考 “Hi3861开发板第一个示例程序.md” 镜像烧录 烧录章节。

## 软件设计



### 连接平台
在连接平台前需要获取CLIENT_ID、USERNAME、PASSWORD，访问[这里](https://iot-tool.obs-website.cn-north-4.myhuaweicloud.com/)，填写[注册设备](https://support.huaweicloud.com/devg-iothub/iot_01_2127.html#ZH-CN_TOPIC_0240834853__zh-cn_topic_0182125275_section108992615509)后生成的设备ID（DeviceId）和密钥（DeviceSecret），生成连接信息（ClientId、Username、Password）。
```c
WifiConnect("CBG", "chinasoft");
    device_info_init(CLIENT_ID, USERNAME, PASSWORD);
    oc_mqtt_init();
    mqtt_shadow_rsp_cb(ocShadowCallback); //注册设备影子回调函数
```

### 推送数据

当需要上传数据时，需要先拼装数据，然后通过oc_mqtt_profile_propertyreport上报数据。代码示例如下： 

![](pic/数据上报.png)


### 影子数据获取

华为IoT平台支持设备影子数据获取，先拼接请求，通过oc_mqtt_profile_getshadow发送请求。接收响应后会通过回调函数将收到的数据发送到队列中，读取队列消息后做后续处理，代码示例如下： 

```c

static void getShadowMsg(void)
{
    int ret;
    oc_mqtt_profile_shadowget_t payload;
    payload.object_device_id = USERNAME;
    char request[10] = {0};
    sprintf(request, "R%d", rand() % 10000);
    payload.request_id = request;
    payload.service_id = "wktmp";
    ret = oc_mqtt_profile_getshadow(CLIENT_ID, &payload);
}

void ocShadowCallback(uint8_t *recv_data, size_t recv_size, uint8_t **resp_data, size_t *resp_size)
{
    app_msg_t *app_msg;

    int ret = 0;
    app_msg = malloc(sizeof(app_msg_t));
    app_msg->msg_type = en_msg_shadow;
    app_msg->msg.cmd.payload = (char *)recv_data;
    app_msg->msg.cmd.len = recv_size;

    ret = osMessageQueuePut(mid_MsgQueue, &app_msg, 0U, 0U);
    if (ret != 0)
    {
        free(recv_data);
    }
    *resp_data = NULL;
    *resp_size = 0;
}
```


## 编译调试


### 登录

设备接入华为云平台之前，需要在平台注册用户账号，华为云地址：<https://www.huaweicloud.com/>

在华为云首页单击产品，找到IoT物联网，单击设备接入IoTDA 并单击立即使用。

![](pic/登录1.png)



### 创建产品

在设备接入页面可看到总览界面，展示了华为云平台接入的协议与域名信息，根据需要选取MQTT通讯必要的信息备用。

接入协议（端口号）：MQTT 1883

选中侧边栏产品页，单击右上角“创建产品”

![](pic/创建产品.png)

在页面中选中所属资源空间，并且按要求填写产品名称，选中MQTT协议，数据格式为JSON，并填写厂商名称，选择所属行业以及添加设备类型，并单击右下角“确定”如图：

![](pic/创建产品2.png)

创建完成后，选择“查看详情”，在产品页会自动生成刚刚创建的产品，单击“查看”可查看创建的具体信息。

单击产品详情页的自定义模型，在弹出页面中新增服务，自定义服务ID：

![](pic/创建产品3.png)

在服务ID的下拉菜单下点击“添加属性”填写相关信息：

![](pic/服务属性.png)



### 注册设备

在侧边栏中单击“设备”，进入设备页面，单击右上角“注册设备”，勾选对应所属资源空间并选中刚刚创建的产品，注意设备认证类型选择“秘钥”，按要求填写秘钥。

![](pic/注册设备1.png)

记录下设备ID和设备密钥

注册完成后，在设备页面单击“所有设备”，即可看到新建的设备，同时设备处于未激活状态。


### 设备绑定
在连接平台前需要获取CLIENT_ID、USERNAME、PASSWORD，访问[这里](https://iot-tool.obs-website.cn-north-4.myhuaweicloud.com/)，填写注册设备时生成的设备ID和设备密钥生成连接信息（ClientId、Username、Password），并将修改代码对应位置。
![](pic/代码修改设备信息.png)





### 联网调试

示例代码编译烧录代码后，按下开发板的RESET按键，通过串口助手查看日志，首先会打印最近一次上报的温度信息，然后打印当前温度信息。

```c
Today is : Wes, pre Temp is:32
Today is : Wes, Temp is:28
```
平台上的设备显示为在线状态，点击设备右侧的“查看”，进入设备详情页面，可看到上报的数据

![](pic/设备.png)

在华为云平台的消息跟踪页面可以查看平台与设备的数据交互：

![](pic/消息跟踪.png)



其中设备重新连接请求影子数据时，平台的下发的信息如下：

![](pic/影子响应.png)



设备侧实现对响应的回调处理即可。




​	