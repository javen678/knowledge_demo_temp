
/*
 * Copyright (c) 2021 KaiHong Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AHT20_H
#define AHT20_H

#include <stdint.h>

#include "iot_i2c.h"

#define AHT20_I2C_IDX  0 
#define AHT20_BAUDRATE 400*1000

typedef struct
{
	float    Lux;			  //光照强度
	float    Humidity;        //湿度
	float    Temperature;     //温度
} AHT20_TYPE;

uint32_t AHT20_Calibrate(void);

uint32_t AHT20_StartMeasure(void);

uint32_t AHT20_GetMeasureResult(float* temp, float* humi);

#endif  // AHT20_H